package redis

import (
	"time"

	"github.com/go-redis/redis/v7"
)

type redisClient struct {
	client *redis.Client
}

var err error

// NewRedisClient returns reference to new redisClient object
func NewRedisClient(host string, db int) *redisClient {
	return &redisClient{
		client: redis.NewClient(&redis.Options{
			Addr: host,
			// DB:       db,
			Password: "",
		}),
	}
}

// Set is functionally same as redis.Set
func (cache *redisClient) Set(key, val string, expires ...interface{}) error {
	if len(expires) == 1 {
		err = cache.client.Set(key, val, expires[0].(time.Duration)).Err()
	} else {
		err = cache.client.Set(key, val, 0).Err()
	}
	return err
}

// Get is functionally same as redis.Get
func (cache *redisClient) Get(key string) string {
	val, _ := cache.client.Get(key).Result()
	return val
}
