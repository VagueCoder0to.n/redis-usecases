package main

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/VagueCoder0to.n/Redis-Usecases/app/redis"
)

func main() {
	fmt.Println("Welcome to Redis-Client Example")

	rc := redis.NewRedisClient("localhost:6379", 0)
	if err := rc.Set("name", "Coder", 0*time.Second); err != nil {
		log.Fatalf("Failed at key set: %v", err)
	}

	for i := 0; i < 10; i++ {
		time.Sleep(time.Second)
		fmt.Println(rc.Get("name"))
	}

	rc2 := redis.NewRedisClient("localhost:6379", 0)
	if err := rc2.Set("name", "Coder", 0*time.Second); err != nil {
		log.Fatalf("Failed at key set: %v", err)
	}

	fmt.Println(rc2.Get("INFO"))
}
